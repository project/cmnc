<?php
/**
 * @file
 * Functions for processing arguments and loading the necessary views.
 */

/**
 * Callback for viewing the results of the block “View: My Notifications".
 */
function _cmnc_services_my_notifications_rendered() {
  global $user;
  $args = array($user->uid);

  return _cmnc_services_views_results('my_notifications', 'block_notification_icon', $args);
}

/**
 * Helper function for loading and rendering the view.
 *
 * @param string $view_id
 *   The machine name of the view to render.
 * @param string $display_id
 *   The machine name of the view display to render.
 * @param array $args
 *   An array of arguments to send to the view.
 *
 * @return string
 *   Returns the rendered view.
 */
function _cmnc_services_views_results($view_id, $display_id, $args = NULL) {
  // Load the view.
  $view = views_get_view($view_id);

  // Remove the more link from the display.
  $view->display[$display_id]->display_options['use_more'] = FALSE;

  // Load the display.
  $view->set_display($display_id);

  // Set the arguments if they're present.
  if (!empty($args)) {
    $view->set_arguments($args);
  }

  // Return the rendered view.
  return $view->preview($display_id, $args);
}
